#!/bin/sh
ME=$(readlink -f "$0")
DIR=$(dirname "$ME")

# Define colors
. $DIR/vars/colors.sh

# Define functions
. $DIR/funs/assert_root.sh
. $DIR/funs/get_shell.sh
. $DIR/funs/gt_result.sh
. $DIR/funs/is_bash.sh
. $DIR/funs/mv_old.sh
. $DIR/funs/on_error.sh
. $DIR/funs/print_args.sh
