Shut
====

General-purpose shell utilities.

Designed for and tested on BusyBox' _ash_ and Debian _bash_ and _dash_.

## Content and usage

**vars** (variables) and **funs** (functions) are designed to be [sourced](https://linuxhandbook.com/source-command)
in other scripts. Instead of sourcing individual files under `vars` or `funs` directories,
it is also possible to source the root file `main.sh`, which itself sources
the most useful files under `vars` and `funs`.

**tools** are executables which may be linked in `/usr/local/bin` or other PATH
directories.

**examples** provide advanced usage examples for some shell features, such as parsing
command line arguments using [getopt](http://man7.org/linux/man-pages/man1/getopt.1.html).

**ideas** are in early stage and are probably not usable as-is.

**tests** require more work, too.

## License

Published under the [MIT license](LICENSE.txt).
