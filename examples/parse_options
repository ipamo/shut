#!/bin/sh
print_help() {
    local help_text="
Parse options using util-linux \"getopt\".
See: http://man7.org/linux/man-pages/man1/getopt.1.html

Unrecognized option(s):
- are printed by getopt, except if -q is passed to getopt
- always generate non-zero return code (even if -q is passed to getopt)
- are always excluded from the output

Usefull adaptation:
- Prefix '-o' with '+': parsing stops at the first non-option (usefull for
  subcommands).

Note:
- Checking getopt return code is not required if unrecognized option are
  tolerated (they will be ignored), or if an error trap has been installed.
"
    echo "$help_text"
}

# -----------------------------------------------------------------------------
ME=$(readlink -f "$0")
DIR=$(dirname "$ME")

# Source dependencies
. "$DIR/../funs/print_args.sh"

# -----------------------------------------------------------------------------
# Parse easy options in a function
#
handle_remaining() {
    local FUNCNAME=CHANGEME # only for non-bash shells
    local options # don't assign local directly (otherwise $? will always be 0)
    options=$(getopt -n "$FUNCNAME" -o "fq" -l "force,quiet" -- "$@")
    [ $? -ne 0 ] && return 1
    eval set -- "$options"

    while true; do
        case "$1" in
            -f|--force)      local force=1 ;;
            -q|--quiet)      local quiet=1 ;;
            # ----- end of option list -----
            --) shift; break ;;
            *)  echo "$FUNCNAME: unhandled option: $1" >&2; return 1 ;;
        esac
        shift # option name
    done

    # ---------------------------------
    printf "%s\n" "-f|--force: $force"
    echo "Remaining arguments: $#"
    print_args "$@"
}

# -----------------------------------------------------------------------------
# Parse main script options
#
NAME=$(basename "$ME")
options=$(getopt -n "$NAME" -o "brgc:hv" -l "color:,help,verbose" -- "$@")
[ $? -ne 0 ] && exit 1
eval set -- "$options"

VERBOSE=0
while true; do
    case "$1" in
        -h|--help)      print_help; exit ;;
        -v|--verbose)   VERBOSE=$(($VERBOSE+1)) ;;
        -b)     COLOR=BLUE ;;
        -r)     COLOR=RED ;;
        -g)     COLOR=GREEN ;;
        -c|--color)
            COLOR=$2
            if [ ! "$COLOR" = "BLUE" ] && [ ! "$COLOR" = "RE D" ] && [ ! "$COLOR" = "GREEN" ]; then
                echo "$NAME: invalid value for --color: $COLOR" >&2
                exit 1
            fi
            shift ;; # option value
        # ----- end of option list -----
        --) shift; break ;;
        *)  echo "$NAME: unhandled option: $1" >&2; exit 1 ;;
    esac
    shift # option name
done

# -----------------------------------------------------------------------------
# Run
#
echo "Parsed options:"
echo "\$VERBOSE: $VERBOSE"
echo "\$COLOR: $COLOR"
echo "\$HELP: $HELP"

handle_remaining -q "$@"
