#!/bin/bash
#
# Helper functions to check paths.
#
if [ -z "$COMMON" ]; then
	COMMON=$(dirname "${BASH_SOURCE[0]}")
fi
if ! type -t "on_error" >/dev/null; then
	source $COMMON/common.sh
fi

path_rootonly() {
	# Parse command line arguments
	local quiet_ok=0
	local quiet_err=0
	local positional=()
	while [[ $# -gt 0 ]]; do
		case $1 in
			--quiet-ok)
				quiet_ok=1
				shift
			;;
			--quiet)
				quiet_ok=1
				quiet_err=1
				shift
			;;
			*)
				positional+=("$1")
				shift
			;;
		esac
	done
	set -- "${positional[@]}"

	# Loop on arguments
	local result=0
	local path	
	for path in "$@"; do
		if [ ! -e "$path" ]; then
			[ $quiet_err -eq 1 ] || echo -e "[${RED}missing${NC}] $path" >&2
			result=$(gt_result 1)
			continue
		fi
			
		local user=`stat -c "%U" "$path"`
		if [ "$user" != "root" ]; then
			[ $quiet_err -eq 1 ] || echo -e "[${RED}invalid${NC}] $path: owner $user (expected root)" >&2
			result=$(gt_result 2)
			continue
		fi

		local perm=`stat -c "%a" "$path"`
		[ -d "$path" ] && local expected=700 || local expected=600
		if [ "$perm" != "$expected" ]; then
			[ $quiet_err -eq 1 ] || echo -e "[${RED}invalid${NC}] $path: perm $perm (expected $expected)" >&2
			result=$(gt_result 3)
			continue
		fi

		if [ ! $quiet_ok -eq 1 ]; then
			if [ -f $path ]; then
				echo -e "[${GREEN}rootonly${NC}] $path: $(stat --printf="%a %U %G" "$path")"
			elif [ -d $path ]; then
				echo -e "[${GREEN}rootonly${NC}] $path: $(ls -lh $path)"
			fi
		fi
	done

	return $result
}

assert_path_rootonly() {
	path_rootonly --quiet-ok $@
	local result=$?
	[ $result -ne 0 ] && exit $result
}

path_exists() {
	# Parse command line arguments
	local quiet_ok=0
	local quiet_err=0
	local positional=()
	while [[ $# -gt 0 ]]; do
		case $1 in
			--quiet-ok)
				quiet_ok=1
				shift
			;;
			--quiet)
				quiet_ok=1
				quiet_err=1
				shift
			;;
			*)
				positional+=("$1")
				shift
			;;
		esac
	done
	set -- "${positional[@]}"

	# Loop on arguments
	local result=0
	local path	
	for path in "$@"; do
		if [ -f $path ]; then
			if [ ! $quiet_ok -eq 1 ]; then
				echo -e "[${GREEN}exists${NC}] $path: $(stat --printf="%a %U %G" "$path")"
			fi
		elif [ -d $path ]; then
			[ $quiet_ok -eq 1 ] || echo -e "[${GREEN}exists${NC}] $path: $(ls -lh $path)"
		else
			[ $quiet_err -eq 1 ] || echo -e "[${RED}missing${NC}] $path" >&2
			result=1
		fi
	done

	return $result
}

assert_path_exists() {
	path_exists --quiet-ok $@
	local result=$?
	[ $result -ne 0 ] && exit $result
}

cmd_exists() {
	# Parse command line arguments
	local quiet_ok=0
	local quiet_err=0
	local positional=()
	while [[ $# -gt 0 ]]; do
		case $1 in
			--quiet-ok)
				quiet_ok=1
				shift
			;;
			--quiet)
				quiet_ok=1
				quiet_err=1
				shift
			;;
			*)
				positional+=("$1")
				shift
			;;
		esac
	done
	set -- "${positional[@]}"

	# Loop on arguments
	local result=0
	local cmd
	local path
	for cmd in "$@"; do
		path=$(command -v $cmd)
		if [ $? -eq 0 ]; then
			[ $quiet_ok -eq 1 ] || echo -e "[${GREEN}exists${NC}] Command: $cmd -> $path"
		else
			result=$?
			[ $quiet_err -eq 1 ] || echo -e "[${RED}missing${NC}] Command: $cmd" >&2
		fi
	done

	return $result
}

assert_cmd_exists() {
	cmd_exists --quiet-ok $@
	local result=$?
	[ $result -ne 0 ] && exit $result
}

file_contains() {
	# Parse command line arguments
	local quiet_ok=0
	local quiet_err=0
	local positional=()
	while [[ $# -gt 0 ]]; do
		case $1 in
			--quiet-ok)
				quiet_ok=1
				shift
			;;
			--quiet)
				quiet_ok=1
				quiet_err=1
				shift
			;;
			*)
				positional+=("$1")
				shift
			;;
		esac
	done
	set -- "${positional[@]}"

	if [ -z "$1" ]; then
		echo -e "${RED}file_contains: missing required argument `file`${NC}" >&2
		return 1
	fi
	local file=$1; shift
	
	if [ -z "$1" ]; then
		echo -e "${RED}file_contains: missing required argument `expected`${NC}" >&2
		return 1
	fi
	local expected=$1; shift

	if [ $# -ne 0 ]; then
		echo -e "${RED}file_contains: unexpected arguments: $@${NC}" >&2
		return 1
	fi

	[ $quiet_err -eq 1 ] && local quiet_err_str="--quiet-err"

	# Check if file exist
	if ! path_exists $quiet_err_str --quiet-ok $file; then
		return $?
	fi

	# Check if file contains
	if grep -w "$expected" $file > /dev/null; then
		[ $quiet_ok -eq 1 ] || echo -e "[${GREEN}contains${NC}] $file: \"$expected\""
		return 0
	else
		[ $quiet_err -eq 1 ] || echo -e "[${RED}doesn't contain${NC}] $file: \"$expected\"" >&2
		return 1
	fi
}

dpkg_installed() {
	# Parse command line arguments
	local quiet_ok=0
	local quiet_err=0
	local positional=()
	while [[ $# -gt 0 ]]; do
		case $1 in
			--quiet-ok)
				quiet_ok=1
				shift
			;;
			--quiet)
				quiet_ok=1
				quiet_err=1
				shift
			;;
			*)
				positional+=("$1")
				shift
			;;
		esac
	done
	set -- "${positional[@]}"

	# Loop on arguments
	local result=0
	local status
	local info
	local package
	for package in "$@"; do
		status=$(dpkg-query -W -f='${db:Status-Status}\n' $package 2> /dev/null)
		if [ "$status" == "installed" ]; then
			info=$(dpkg-query -W -f='${Package}: ${Version} ${db:Status-Status}\n' $package 2> /dev/null)
			[ $quiet_ok -eq 1 ] || echo -e "[${GREEN}exists${NC}] Package: $info"
		else
			status=${status:-missing}
			[ $quiet_err -eq 1 ] || echo -e "[${RED}$status${NC}] Package: $package" >&2
			result=1
		fi
	done

	return $result
}

service_active() {
	# Parse command line arguments
	local quiet_ok=0
	local quiet_err=0
	local positional=()
	while [[ $# -gt 0 ]]; do
		case $1 in
			--quiet-ok)
				quiet_ok=1
				shift
			;;
			--quiet)
				quiet_ok=1
				quiet_err=1
				shift
			;;
			*)
				positional+=("$1")
				shift
			;;
		esac
	done
	set -- "${positional[@]}"

	# Loop on arguments
	local result=0
	local status
	local service
	for service in "$@"; do
		status=$(systemctl is-active $service)
		if [ $? -eq 0 ]; then
			[ $quiet_ok -eq 1 ] || echo -e "[${GREEN}$status${NC}] Service: $service"
		else
			status=${status:-missing}
			[ $quiet_err -eq 1 ] || echo -e "[${RED}$status${NC}] Service: $service" >&2
			result=1
		fi
	done

	return $result
}
