
#
# Get version either from git repository or from '.version' file.
#
# WARNING: this is a probably-non-working stub. Things to consider:
# - How to integrate in Dockerfiles without constantly rebuilding images?
# - What happens if last commit was tagged but we made changed in working directory?
# 		=> should not show the tag
#		=> also, ensure that the hash is different that the one of the last commit
# - Improve output: version+hash
#
get_version() {
	local FUNCNAME=get_version
	local options
    options=$(getopt -n "$FUNCNAME" -o "f" -l "full" -- "$@")
    [ $? -ne 0 ] && return 1
    eval set -- "$options"

    while true; do
        case "$1" in
            -f|--full)      local full=1 ;;
            # ----- end of option list -----
            --) shift; break ;;
            *)  printf "$RED%s$NC\n" "$FUNCNAME: unhandled option: $1" >&2; return 1 ;;
        esac
        shift # option name
    done

    # ---------------------------------
	local dir=$1
	if [ -z "$dir" ]; then
		# use comshell
		dir=/usr/share/comshell
	fi

	if [ ! -d "$dir" ]; then
		printf "$RED%s$NC\n" "get_version: \$dir does not exist: $dir" >&2
		return 1
	fi

	local version
	local version_hash

	# If .version file is available, get version from it
	if [ -f "$dir/.version" ]; then
		version=$(cat "$dir/.version" | head -n 1)
		if [ -z "$version" ]; then
			printf "$RED%s$NC\n" "get_version: $dir/.version is empty" >&2
			return 1
		fi
		if [ "$full" = "1" ]; then
			echo "$version $version_hash"
		else
			echo "$version"
		fi
		return 0
	fi

	# If git is available and $dir is in git repository,
	# determine version from tags or from hash.
	version=$(git -C "$dir" tag -l --points-at HEAD | grep -E "^[0-9]+\.[0-9]+\.[0-9](\-.*)?$" -m 1)
	if [ $? -ne 0 ]; then
		printf "$RED%s$NC\n" "get_version: no .version or git in $dir" >&2
		return 1
	fi

	version=${version:-HEAD}
	if [ "$full" = "1" ]; then
		version_hash=$(git -C "$dir" rev-parse HEAD)
		echo "$version $version_hash"
	else
		echo "$version"
	fi
}