#!/bin/sh
docker_network_exists() {
    local name="$1"
    docker network inspect -f "{{.Name}}" "$name" >/dev/null 2>&1
}
