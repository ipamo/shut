#!/bin/sh
#
# Callback function for trapped errors.
#
# USAGE
#
# - Bash:
#		trap "on_error ${FUNCNAME[0]} $NAME:$LINENO" ERR
# - Dash:
#		set -e; trap "on_error $NAME" EXIT
# - One-liner without reference to common.sh:
#		set -e; trap "code=\$?; [ \$code -eq 0 ] || printf \"\033[0;31m%s\033[0m\n\" \"Exiting on code \$code\" >&2" EXIT
#
# REMARK
#
# According to various sources on the Internet, ERR is not a standard trap
# and is only supported by the Korn Shell - which seemed to invent it - and
# Bash, which seemed to adopt it.
# https://github.com/bmizerany/roundup/issues/25#issuecomment-10978764
#
on_error() {
	# Prepare message and exit code
	local code=$?
	if [ $code -eq 0 ]; then
		exit 0
	fi

	local message="Exiting on code $code"

	for arg in "$@"; do
		message="$message in $arg"
	done

	# Print message and exit
	printf "$RED%s$NC\n" "$message" >&2
    exit $code
}
