#!/bin/sh
#
# Archive a file or directory by renaming it with '-old' suffix
#
# Options:
# 	-f, --force   	Ignore if path to archive does not exist.
# 	-q, --quiet		Do not display move.
#
mv_old() {
    local FUNCNAME=mv_old
	local options
    options=$(getopt -n "$FUNCNAME" -o "f" -l "force" -- "$@")
    [ $? -ne 0 ] && return 1
    eval set -- "$options"

    while true; do
        case "$1" in
            -f|--force)      local force=1 ;;
            -q|--quiet)      local quiet=1 ;;
            # ----- end of option list -----
            --) shift; break ;;
            *)  printf "$RED%s$NC\n" "$FUNCNAME: unhandled option: $1" >&2; return 1 ;;
        esac
        shift # option name
    done

    # ---------------------------------
	# Parse arguments
	local path=$1; 
    [ -z "$path" ] && printf "$RED%s$NC\n" "$FUNCNAME: argument missing: \$path" >&2 && return 1
    shift
    local suffix=${1:-"-old"}

	# Check existency of input path
	if [ ! -e "$path" ]; then
		[ "$force" = "1" ] && return 0 
		printf "$RED%s$NC\n" "$FUNCNAME: path missing: $path" >&2
		return 1
	fi

	# Prepare target name
	local n=1
	local path_old="$path-old"
	while [ -e "$path_old" ]; do
		n=$(($n + 1))
		path_old="$path-old$n"
	done

	# Perform move
	[ "$quiet" = "1" ] || echo "$FUNCNAME: $path -> $path_old"
	mv "$path" "$path_old"
}
