#!/bin/sh
#
# Detect if the currently executing shell seems to be bash.
#
# See also: get_shell
#
is_bash() {
	[ "$(readlink -f /proc/$$/exe)" = "/bin/bash" ] && return 0 || return 1
}
