#!/bin/sh
#
# Print arguments.
# Used to debug argument and option parsings.
#
# Example:
#     print_args "$@"
#
print_args() {
    local nargs=$#
    local n=1
    for arg in "$@"; do
        echo "\$$n: $arg"
        n=$(($n+1))
    done
}
