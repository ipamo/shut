#!/bin/sh
#
# Ensure current script is run as root.
#
assert_root() {
	if [ $(id -u) -ne 0 ]; then
		printf "$RED%s$NC\n" "This script must be run as root." >&2
		exit 1
	fi
}
