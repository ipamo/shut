#!/bin/sh
#
# Print the path of the currently executing shell.
# Typically: "/bin/bash", "/bin/dash" or "/bin/busybox" (for BusyBox' Ash).
#
# Works on Linux, not sure if it is portable to FreeBSD, OS X, Cygwin.
#
# See also: is_bash
#
get_shell() {
	readlink -f /proc/$$/exe
}
