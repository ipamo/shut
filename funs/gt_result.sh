#
# Print return code of last command if it is greater than variable $result.
# If argument is provided, it is used instead of return code of last command.
#
# Usage example:
#     local result=0
#     do_something; result=$(gt_result)
#
gt_result() {
	local last=$?
	[ -n "$1" ] && last=$1
	
	if [ "$last" -gt "$result" ]; then
		echo $last
	else
		echo $result
	fi
}
